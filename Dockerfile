FROM  registry.cern.ch/acc/acc-py_el9_ci:2023.06

ADD . /opt/lhc-coupling-analysis-service/
WORKDIR /opt/lhc-coupling-analysis-service/

RUN pip install -r requirements.txt

# Run test here to prevent image creation in case of problems
RUN python /opt/lhc-coupling-analysis-service/lhc_coupling_analysis_test.py

CMD ["python", "/opt/lhc-coupling-analysis-service/lhc_coupling_service_cli.py"]
