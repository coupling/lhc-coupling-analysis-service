# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
import grpc

import coupling_pb2 as coupling__pb2


class CouplingCalculatorStub(object):

  def __init__(self, channel):
    """Constructor.

    Args:
      channel: A grpc.Channel.
    """
    self.CalculateTransverseCoupling = channel.unary_unary(
        '/couplingservice.CouplingCalculator/CalculateTransverseCoupling',
        request_serializer=coupling__pb2.TransverseCouplingRequest.SerializeToString,
        response_deserializer=coupling__pb2.TransverseCouplingResponse.FromString,
        )


class CouplingCalculatorServicer(object):

  def CalculateTransverseCoupling(self, request, context):
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')


def add_CouplingCalculatorServicer_to_server(servicer, server):
  rpc_method_handlers = {
      'CalculateTransverseCoupling': grpc.unary_unary_rpc_method_handler(
          servicer.CalculateTransverseCoupling,
          request_deserializer=coupling__pb2.TransverseCouplingRequest.FromString,
          response_serializer=coupling__pb2.TransverseCouplingResponse.SerializeToString,
      ),
  }
  generic_handler = grpc.method_handlers_generic_handler(
      'couplingservice.CouplingCalculator', rpc_method_handlers)
  server.add_generic_rpc_handlers((generic_handler,))
