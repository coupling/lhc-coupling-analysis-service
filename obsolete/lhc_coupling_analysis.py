"""
Created on Mar 1, 2017

@author: mihostet
"""

import logging as log
import os
from obsolete.staging import run_python_stage, stage_in, run_binary_stage, assert_nonempty_output_file, stage_clear, \
    BETA_BEAT_PROJECT_PATH
import threading


def run_svd_clean(stage_dir, sdds_file, beam):
    cleaned_sdds_file = sdds_file + ".new"
    run_python_stage(stage_dir, "Analyses/svd_clean.py", turn=1, maxturns=9500, p=0.00001,
                     max__peak__cut=20, sumsquare=0.925, sing_val=12,
                     file=sdds_file, output=cleaned_sdds_file,
                     accel="LHCB%d" % beam, resync=True)
    assert_nonempty_output_file(cleaned_sdds_file)
    return cleaned_sdds_file



def runCleanAndHarpy(stage_dir, sdds_file, beam, twiss_file, tunes):
    run_python_stage(stage_dir, "hole_in_one.py", first_argument='clean',second_argument='harpy', file=sdds_file, model=twiss_file, harpy_mode='svd'
        , outputdir=stage_dir,tunex=tunes['driven']['x'],tuney=tunes['driven']['y'], nattunex=tunes['natural']['x'], nattuney=tunes['natural']['y'], sing_val=8, skip_files=True)


    assert_nonempty_output_file(sdds_file + ".linx")
    assert_nonempty_output_file(sdds_file + ".liny")



# /afs/cern.ch/eng/sl/lintrack/Beta-Beat.src/hole_in_one/hole_in_one.py --f=Beam2\@Turn\@2015_04_09\@01_28_10_125.sdds --model=../../../models/LHCB2/b2_inj/twiss.dat -c --outputdir=test


def run_convert_and_svd_clean(stage_dir, sdds_file, twiss_file):


    # os.system('ptyhon2 ' + stage_dir+'hole_in_one/hole_in_one.py  file=' + sdds_file + 'model=' +twiss_file + ' ' + 'outputdir='+ stage_dir, ' --write_clean' )
    run_python_stage(stage_dir, "hole_in_one.py", first_argument='clean', file=sdds_file, model=twiss_file,
                     outputdir=stage_dir,
                     write_clean=True)

    cleaned_sdds_file = sdds_file + ".clean"
    assert_nonempty_output_file(cleaned_sdds_file)
    return cleaned_sdds_file


def run_drive(stage_dir, sdds_file, beam, tunes, turns, num_threads=12):
    with open(stage_dir + "/DrivingTerms", 'w') as drive_input:
        drive_input.write("%s %d %d\n" % (sdds_file, 1, turns))
    with open(stage_dir + "/Drive.inp", 'w') as drive_input:
        drive_input.write("KICK=1\n")
        drive_input.write("CASE(1[H], 0[V])=1 \n")
        drive_input.write("KPER(KICK PERCE.)=0.5 \n")
        drive_input.write("TUNE X=%.5f \n" % tunes['driven']['x'])
        drive_input.write("TUNE Y=%.5f \n" % tunes['driven']['y'])
        drive_input.write("PICKUP START=0 \n")
        drive_input.write("PICKUP END=520 \n")
        drive_input.write("ISTUN=0.01 \n")
        drive_input.write("LABEL RUN (1[yes])=0 \n")
        drive_input.write("WINDOWa1=0.04 \n")
        drive_input.write("WINDOWa2=0.1 \n")
        drive_input.write("WINDOWb1=0.4 \n")
        drive_input.write("WINDOWb2=0.45 \n")
        drive_input.write("NATURAL X=%.5f \n" % tunes['natural']['x'])
        drive_input.write("NATURAL Y=%.5f \n" % tunes['natural']['y'])
    run_binary_stage(stage_dir, "drive/Drive_God_lin", stage_dir, OMP_NUM_THREADS=num_threads)
    assert_nonempty_output_file(sdds_file + "_linx")
    assert_nonempty_output_file(sdds_file + "_liny")


def run_get_llm(stage_dir, sdds_file, beam, twiss_file):
    output_dir = stage_dir + "/GetLLM/"
    run_python_stage(stage_dir, "GetLLM/GetLLM.py", accel="LHCB%d" % beam, model=twiss_file, files=sdds_file,
                     output=output_dir,
                     tbtana="SUSSIX", bpmu="mm", lhcphase=1, threebpm=1, coupling=1)
    assert_nonempty_output_file(output_dir + "getcouple_free.out")
    return output_dir


def run_correct_coupling(stage_dir, llm_dir, beam):
    run_python_stage(stage_dir, "correction/correct_coupleDy.py", accel="LHCB%d" % beam, path=llm_dir, cut=0.01,
                     errorcut="0.02,0.02",  MinStr=0.000001, Dy="1,1,0,0,0",
                     opt=stage_dir, Variables="coupling_knobs", rpath=BETA_BEAT_PROJECT_PATH)
    output_tfs = llm_dir + "changeparameters_couple.tfs"
    assert_nonempty_output_file(output_tfs)
    return output_tfs


def run_create_model(stage_dir, beam, modifier, run_mode, nat_tune_x, nat_tune_y, driven_tune_x, driven_tune_y):
    run_python_stage(stage_dir, "/model/creator.py", type='nominal', lhcmode=run_mode, beam=beam,
                     optics=modifier,
                     acd=True, nattunex=nat_tune_x,
                     nattuney=nat_tune_y, drvtunex=driven_tune_x, drvtuney=driven_tune_y, fullresponse=True,
                     output=stage_dir, accel='lhc')


def run_create_correctionModel(stage_dir, beam, modifier, run_mode, nat_tune_x, nat_tune_y):
    run_python_stage(stage_dir, "/model/creator.py", type='coupling_correction', lhcmode=run_mode, beam=beam,
                     optics=modifier,
                     nattunex=nat_tune_x,
                     nattuney=nat_tune_y,
                     output=stage_dir + '/GetLLM/', accel='lhc', writeto=stage_dir + '/job.twiss.madx')
    
    
def run_getPredictedCorrection(stage_dir):    
    run_python_stage(stage_dir,"/correction/getdiff.py", first_argument=stage_dir + '/GetLLM/')
                         
                         
def read_correction_tfs(corrections_tfs):
    with open(corrections_tfs) as tfs:
        for line in tfs:
            parts = [p for p in line.split(' ') if p]
            if 're_ip7_knob' in parts[0]:
                real = float(parts[1])
            if 'im_ip7_knob' in parts[0]:
                imag = float(parts[1])
    return complex(real, imag)



def write_modifier_to_dir(stage_dir, optics, horTune):
    filename = stage_dir + '/modifiers.madx'
    print("This is the optics modifier file: ", optics)
    if ".madx" not in optics:
        if horTune < 0.29:
            optics = '/strength/ats/low-beta/ats_11m_fixQ8L4.madx'
            print('using injection optics')
        else:
            optics = '/strength/ats/low-beta/ats_30cm_ctpps2.madx'
            print('using 30cm Optics ')

    file_object = open(filename, 'w')
    mod_strs = optics.split('&')
    modelpath = os.getcwd() + '/model_rep/'
    print(stage_dir)

    for mod in mod_strs:
        strength_path = modelpath + mod.strip(" ")
        if(os.path.isfile(strength_path)):
            file_object.write('call, file=' + '"' + strength_path + '";' + "\n")
        else:
            file_object.write('call, file=' + '"' + modelpath + '/strength/ats/low-beta/ats_11m_fixQ8L4.madx' + '";' + "\n")

    file_object.close()
    return filename


def generate_full_response(stage_dir, beam):
    to_full_response_core = BETA_BEAT_PROJECT_PATH + '/correction/fullresponse/'
    run_python_stage(stage_dir, "/correction/fullresponse/generateFullResponse_parallel.py", accel='LHCB' + str(beam)
                     , path=stage_dir, deltak=0.00002, deltakl=0.0, core=to_full_response_core, f=True)


def full_model_creation(stage_dir, beam, model, optics, tunes, maximum_threads=8):

    modif = write_modifier_to_dir(stage_dir, optics, tunes['natural']['x'])
    
    run_create_model(stage_dir, beam, modifier=modif, run_mode=model, nat_tune_x=tunes['natural']['x']
                     ,nat_tune_y=tunes['natural']['y'], driven_tune_x=tunes['driven']['x'],
                     driven_tune_y=tunes['driven']['y'])
  
    
    return modif


def full_analysis(sdds_file, beam, number_of_turns, model, optics, tunes, maximum_threads=128, clean_tmp=True):
    sussix = False
    if beam == 1:
        lookup_table = os.getcwd() + "/stand_alone_test/model/LHCB1/twiss.dat"
    elif beam == 2:
        lookup_table = os.getcwd() + "/stand_alone_test/model/LHCB2/twiss.dat"
    else:
        raise ValueError("Invalid beam number: " + beam)

    staged_files, stage_dir = stage_in({'sdds': sdds_file}, "name")
    staged_sdds_file = staged_files['sdds']
    
    modelThread = threading.Thread(target=full_model_creation, args=(stage_dir, beam, model, optics, tunes))
    modelThread.start()


    if(sussix):        
        cleaned_sdds_file = run_convert_and_svd_clean(stage_dir, staged_sdds_file, lookup_table)
    
        run_drive(stage_dir, sdds_file=cleaned_sdds_file, beam=beam, tunes=tunes, turns=number_of_turns,
              num_threads=maximum_threads)
    else:
        runCleanAndHarpy(stage_dir, sdds_file=staged_sdds_file, beam=beam, twiss_file=lookup_table, tunes=tunes)
        cleaned_sdds_file = staged_sdds_file


    modelThread.join()


    fullResponseThread = threading.Thread(target=generate_full_response, args=(stage_dir, beam))
    fullResponseThread.start()
    get_llm_output_dir = run_get_llm(stage_dir, sdds_file=cleaned_sdds_file, beam=beam,
                                     twiss_file=stage_dir + '/twiss.dat')


    fullResponseThread.join()

    corrections_tfs = run_correct_coupling(stage_dir, llm_dir=get_llm_output_dir, beam=beam)
    
    #This has to be uncommented when we want to send the information back to the operator. 
    #run_create_correctionModel(stage_dir, beam, pathToModif, model, nat_tune_x=tunes['natural']['x']
    #                 ,nat_tune_y=tunes['natural']['y'])
    #run_getPredictedCorrection(stage_dir)

    log.info("All stages done")

    corrections = read_correction_tfs(corrections_tfs)
    log.info("Coupling corrections found: %s", corrections)

    if clean_tmp:
        stage_clear(stage_dir)

    return corrections
