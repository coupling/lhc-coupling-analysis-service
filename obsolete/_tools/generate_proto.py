from grpc_tools import protoc
import urllib3
import os
import shutil
import sys
from zipfile import ZipFile

PROTO_CONTAINER_FILE_NAME = 'protos.zip'
TMP_DIR = './proto_tmp'
PROTO_DIR = './grpc_proto'
PACKAGE_INIT_TPL = './_tools/package_init.py.tpl'

http = urllib3.PoolManager()


def clean_and_create_folder(path):
    delete_if_exists(path)
    os.makedirs(path)


def delete_if_exists(path):
    if os.path.isdir(path):
        shutil.rmtree(path)


def download_file(url):
    print('Downloading ' + url)
    response = http.request('GET', url)
    downloaded_content = response.data
    file_dst_path = TMP_DIR + '/' + PROTO_CONTAINER_FILE_NAME
    with open(file_dst_path, 'wb') as f:
        f.write(downloaded_content)
    return file_dst_path


def compile_protos_in_zip(zip_file_path, out_dir):
    with ZipFile(zip_file_path, 'r') as zip_file:
        for inner_file_info in zip_file.infolist():
            if inner_file_info.filename.endswith('.proto'):
                zip_file.extract(inner_file_info, path=TMP_DIR)
                print('Compiling ' + inner_file_info.filename)
                compile_proto(TMP_DIR + '/' + inner_file_info.filename, TMP_DIR, out_dir)


def compile_proto(proto_file_path, proto_dir, out_dir):
    protoc.main(
        (
            '',
            '--python_out=' + out_dir,
            '--grpc_python_out=' + out_dir,
            '--proto_path=' + proto_dir,
            proto_file_path
        )
    )


def copy_package_init(dst_folder):
    shutil.copyfile(PACKAGE_INIT_TPL, dst_folder + '/__init__.py')


if __name__ == '__main__':
    url = sys.argv[1]
    dst_folder = sys.argv[2] if len(sys.argv) > 2 else PROTO_DIR

    clean_and_create_folder(TMP_DIR)
    clean_and_create_folder(dst_folder)

    tmp_protos_file_path = download_file(url)
    compile_protos_in_zip(tmp_protos_file_path, dst_folder)
    copy_package_init(dst_folder)
    delete_if_exists(TMP_DIR)
