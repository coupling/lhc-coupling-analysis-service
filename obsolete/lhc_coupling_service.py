import logging as log
import os
import sys
import shutil
import tempfile
import time
import grpc
from concurrent import futures
from obsolete.staging import assert_correct_environment
from obsolete.grpc_proto import coupling_pb2 as grpc_domain, coupling_pb2_grpc as grpc_server
from obsolete.lhc_coupling_analysis import full_analysis

ONE_DAY_IN_SECONDS = 60 * 60 * 24
LISTEN_ADDRESS = '[::]:60051'
MAX_MESSAGE_LENGTH_BYTES = 1000000000
DEMO_MODE = False

def log_request(request):
    log.info("Got request: %s, %s, %s, %s, %s, %s, %s",
             "BEAM " + str(request.beam + 1),
             "number_of_turns" + str(request.number_of_turns),
             "tune_driven_x" + str(request.tune_driven_x),
             "tune_driven_y" + str(request.tune_driven_y),
             "tune_natural_x" + str(request.tune_natural_x),
             "tune_natural_y" + str(request.tune_natural_y),
             "optics" + str(request.optics)
             )

class CouplingServiceImpl(grpc_server.CouplingCalculatorServicer):
    def CalculateTransverseCoupling(self, request, context):
        log_request(request)
        log.info("Starting calculation...")
        temp_sdds_dir = None
        if DEMO_MODE:
            log.info(
                "This request will run in DEMO mode. The sdds file path on the request will NOT be taken into account")
            if request.beam == 0:
                sdds_file_path = os.getcwd() + "/stand_alone_test/data/Beam1_Turn_2016_04_21_08_18_08_754.sdds"
            else:
                sdds_file_path = os.getcwd() + "/stand_alone_test/data/Beam2@Turn@2016_10_31@00_03_38_207.sdds"
        else:
            temp_sdds_dir = tempfile.mkdtemp(prefix="lhc-online-coupling-analysis-sdds-")
            sdds_file_path = temp_sdds_dir + '/currentFile.sdds'
            with open(sdds_file_path, 'wb') as f:
                f.write(request.sdds_file)

        correction = full_analysis(sdds_file=sdds_file_path,
                                   beam=request.beam + 1,
                                   # Not sure about this... but changed it to be consistent with the tests
                                   number_of_turns=request.number_of_turns,
                                   model="lhc_runII_2018",
                                   optics=request.optics,
                                   tunes={'driven': {'x': request.tune_driven_x, 'y': request.tune_driven_y},
                                          'natural': {'x': request.tune_natural_x, 'y': request.tune_natural_y}},
                                   maximum_threads=128)
        if temp_sdds_dir is not None:
            log.info("Cleaning up SDDS file in '%s'" % temp_sdds_dir)
            shutil.rmtree(temp_sdds_dir, ignore_errors=True)
        log.info("Returning correction: %s", correction)
        return grpc_domain.TransverseCouplingResponse(real=correction.real, imaginary=correction.imag)


def serve():
    assert_correct_environment()
    log.info("LHC coupling service starting ...")
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=1),
                         options=(('grpc.max_receive_message_length', MAX_MESSAGE_LENGTH_BYTES,),))
    grpc_server.add_CouplingCalculatorServicer_to_server(CouplingServiceImpl(), server)
    server.add_insecure_port(LISTEN_ADDRESS)
    server.start()
    log.info("LHC coupling startup complete @ %s", LISTEN_ADDRESS)
    try:
        while True:
            time.sleep(ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)


if __name__ == '__main__':
    log.basicConfig(level=log.INFO)
    if len(sys.argv) >= 2 and sys.argv[1] == '--demo':
        log.info("Starting in demo mode")
        DEMO_MODE = True

    serve()
