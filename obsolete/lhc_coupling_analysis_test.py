import unittest
from obsolete import lhc_coupling_analysis as lca
import os
import logging as log
from timeit import default_timer as timer


class TestLhcCouplingAnalysis(unittest.TestCase):
    """
    Simple unit test. It does not check the exact correctness of the calculation, rather that the calculations is executed and is finishing without errors.
    """
 
    MAX_EXPECTED_DURATION_SECONDS = 660
    ACURRACY_DECIMAL_PLACES = 5

    def test_reference_result_and_duration(self):
        # Test Beam 1
        
        request = {'sdds_file': os.getcwd() + "/stand_alone_test/data/Beam1@Turn@2017_05_22@15_20_06_877.sdds",
                   'beam': 1, "number_of_turns": 6600,
                   'model': "lhc_runII_2018",
                   'optics': "dummy_optics",
                   'tunes': {'driven': {'x': 0.255, 'y': 0.31}, 'natural': {'x': 0.275, 'y': 0.295}}}

        start = timer()
        corrections = lca.full_analysis(**request)
        end = timer()
        duration_in_seconds = end - start
        self.assertTrue(duration_in_seconds < self.MAX_EXPECTED_DURATION_SECONDS,
                        "Maximal duration should be below {0} seconds, but was {1} seconds.".format(
                            self.MAX_EXPECTED_DURATION_SECONDS, duration_in_seconds))
        self.assertAlmostEqual(corrections.real, 0.00209279588293, self.ACURRACY_DECIMAL_PLACES)
        self.assertAlmostEqual(corrections.imag, -0.00145915205144, self.ACURRACY_DECIMAL_PLACES)
        
        
        # Test Beam 2
        request = {'sdds_file': os.getcwd() + "/stand_alone_test/data/Beam2@Turn@2017_05_08@17_32_29_400.sdds",
                   'beam': 2, "number_of_turns": 6600,
                   'model': "lhc_runII_2018",
                   'optics': "dummy_optics",
                   'tunes': {'driven': {'x': 0.255, 'y': 0.31}, 'natural': {'x': 0.27, 'y': 0.295}}}

        start = timer()
        corrections = lca.full_analysis(**request)
        end = timer()
        duration_in_seconds = end - start
        self.assertTrue(duration_in_seconds < self.MAX_EXPECTED_DURATION_SECONDS,
                        "Maximal duration should be below {0} seconds, but was {1} seconds.".format(
                            self.MAX_EXPECTED_DURATION_SECONDS, duration_in_seconds))
        self.assertAlmostEqual(corrections.real, 0.0101037968521, self.ACURRACY_DECIMAL_PLACES)
        self.assertAlmostEqual(corrections.imag, -0.000102090797412, self.ACURRACY_DECIMAL_PLACES)

        # def suite():
        #   suite = unittest.TestSuite()
        #  suite.addTest(TestLhcCouplingAnalysis('test_reference_result_and_duration'))
        # return suite


if __name__ == '__main__':
    log.basicConfig(level=log.INFO)
    unittest.main()
