import os
import tempfile

from flask import Flask, request
from werkzeug.utils import secure_filename
from lhc_coupling_analysis import calculate_correction_analysis

app = Flask(__name__)
app.config['MAX_CONTENT_LENGTH'] = 1 * 1024 * 1024 * 1024  # 1GB


@app.errorhandler(Exception)
def error_handling(e):
    return str(e), 500


@app.route('/calculate-coupling/', methods=['POST'])
def calculate_coupling():
    try:
        with tempfile.TemporaryDirectory() as session_tmp_dir:
            sdds_file = request.files['sdds-file']
            filename = secure_filename(sdds_file.filename)
            upload_path = os.path.join(session_tmp_dir, filename)
            sdds_file.save(upload_path)

            app.logger.info("Got request: %s, %s, %s, %s, %s, %s",
                            "BEAM " + str(request.form['beam']),
                            "tune_driven_x " + str(request.form['tune_driven_x']),
                            "tune_driven_y " + str(request.form['tune_driven_y']),
                            "tune_natural_x " + str(request.form['tune_natural_x']),
                            "tune_natural_y " + str(request.form['tune_natural_y']),
                            "mad-model_uri " + str(request.form['mad-model_uri'])
                            )

            analysis_input = {
            "sdds_file" : upload_path,
            "beam" : int(request.form['beam']),
            "tune_driven_x" : float(request.form['tune_driven_x']),
            "tune_driven_y" : float(request.form['tune_driven_y']),
            "tune_natural_x" : float(request.form['tune_natural_x']),
            "tune_natural_y" : float(request.form['tune_natural_y']),
            "excitation_device" : "adt",
            "mad_model_uri" : str(request.form['mad-model_uri']),
            "session_tmp_dir": session_tmp_dir
            }
            correction = calculate_correction_analysis(
            **analysis_input
            )

            return {
                'beam': str(request.form['beam']),
                'real': correction["real"],
                'imaginary': correction["imaginary"]
            }
    except Exception as e:
        return "Error while calculating coupling: " + str(e), 500


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True, port=60051)
