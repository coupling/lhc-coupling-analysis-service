"""
Created on Mar 1, 2017

@author: mihostet
"""
import logging as log
import os
import shutil
import sys
import tempfile
import datetime
from subprocess import check_call, CalledProcessError
from timeit import default_timer as timer

EXTERNAL_STAGE_TIMEOUT_SEC = 360

BETA_BEAT_PROJECT_PATH = os.environ['BETA_BEAT_PROJECT_PATH']
sys.path.append(BETA_BEAT_PROJECT_PATH)

PYTHON_2_BINARY = os.getenv('PYTHON_2_BINARY', 'python2')
PYTHON_3_BINARY = os.getenv('PYTHON_3_BINARY', 'python3')


def assert_correct_environment():
    if shutil.which(PYTHON_2_BINARY) is None:
        raise RuntimeError(
            'Python 2 executable not found ({}). Set PYTHON_2_BINARY for custom paths.'.format(PYTHON_2_BINARY))

    if shutil.which(PYTHON_3_BINARY) is None:
        raise RuntimeError(
            'Python 3 executable not found ({}). Set PYTHON_3_BINARY for custom paths.'.format(PYTHON_3_BINARY))


def run_python_stage(stage_dir, filename, first_argument='',second_argument='', expand_true_to_flags=True, **arguments):
    def generate_argument(key, value):
        arg = "--%s" % key.replace('__', '-')
        if expand_true_to_flags and value is True:
            return arg
        if expand_true_to_flags and value is False:
            return None
        arg += '=' + str(value)
        return arg

    if len(second_argument) > 2:
        command = [PYTHON_2_BINARY, BETA_BEAT_PROJECT_PATH + filename, first_argument, second_argument]
    elif len(first_argument) > 2:
        command = [PYTHON_2_BINARY, BETA_BEAT_PROJECT_PATH + filename, first_argument]
    else:
        command = [PYTHON_2_BINARY, BETA_BEAT_PROJECT_PATH + filename]

    command += [generate_argument(k, v) for k, v in arguments.items()]
    log.info("Running python stage: %s", command)

    output_log_file = stage_dir + '/' + os.path.basename(filename) + '.out.log'
    start_time = timer()
    print("command", command)
    run_command(command, output_log_file)
    end_time = timer()

    log.info("PROFILING: python stage '%s' elapsed %.3fs", filename, (end_time - start_time))


def run_binary_stage(stage_dir, filename, *args, **environment):
    command = [BETA_BEAT_PROJECT_PATH + filename] + list(args)
    log.info("Running binary stage: %s", command)

    merged_environment = os.environ
    if environment is not None:
        for k, v in environment.items():
            merged_environment[str(k)] = str(v)

    output_log_file = stage_dir + '/' + os.path.basename(filename) + '.out.log'
    start_time = timer()
    run_command(command, output_log_file)
    end_time = timer()

    log.info("PROFILING: binary stage '%s' elapsed %.3fs", filename, (end_time - start_time))


def run_command(command, output_log_file):
    try:
        with open(output_log_file, 'w') as output_log:
            check_call(command, stderr=output_log, stdout=output_log, timeout=EXTERNAL_STAGE_TIMEOUT_SEC, env={})
    except CalledProcessError:
        output_file_handle = open(output_log_file, 'r')
        output_file_contents = output_file_handle.read()
        output_file_handle.close()
        raise Exception("Error while running {}: {}".format(command, output_file_contents))


def assert_nonempty_output_file(filename):
    if not os.path.isfile(filename):
        raise ValueError("The output file '{}' does not exist after executing the current stage!".format(filename))
    if not os.stat(filename).st_size > 0:
        raise ValueError("The output file '{}' was found empty executing the current stage!".format(filename))

    log.info("the expected output file '%s' is present and non-empty, proceeding", filename)


def stage_in(file_list,dirName):
    stage_dir = tempfile.mkdtemp(prefix="lhc-online-coupling-analysis-" + datetime.datetime.now().isoformat() + "-")
    #stage_dir = "
    staged_files = {}
    for label, filename in file_list.items():
        dest_filename = stage_dir + "/" + os.path.basename(filename)
        log.info("Staging in '%s' :: %s => %s", label, filename, dest_filename)
        shutil.copy2(filename, dest_filename)
        staged_files[label] = dest_filename
    return staged_files, stage_dir


def stage_clear(stagedir):
    log.info("Deleting stage dirs ")
    shutil.rmtree(stagedir, ignore_errors=True)
