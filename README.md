# Coupling analysis correction project.

This project contains the code to run a coupling correction analysis in operations.
It uses [omc3](https://github.com/pylhc/omc3) repository and is based on Anaconda.

### How to update omc3 version?

omc3 library is cloned into the container at build time. To customize the version to be used, modify the `Dockerfile`

### How to release this project?

Create a tag with a proper semantic version (e.g. `1.2.1`).
The CI will start and build a container for you.
**Note**: make sure to include a meaningful description on the tag as release note.

Once finished, the BE-CSS importer will make the container available in the TN.

**Note 1**: the above works if the container synced in the TN has the `:latest` tag. Java server will pull it at startup.
Having versioned tags is definitely a desired outcome but they are not yet supported by the synchronization process (as of 02/2022)

**Note 2**: unfortunately the Gitlab to TN registry synchronization takes a around 15min... (tested 02/2022)

### Note on rootless containers

Starting Run3, BE-CSS supports rootless container via Podman on servers managed by them.

This poses a problem when running the container with a inner user (e.g. `coupling-user`) and wanting to share a volume.
The volume will be mounted in the host file system and will be created by the host user. Inside the container, the folder
will have root permission and therefore not accessible by the inner user (e.g. `coupling-user`).
This is because the root user inside the container *is* the user who runs the container in the host machine (`podman run ...`).

Since rootless containers are a good idea in general, the inner user (`coupling-user`) is now dropped since 0.5.0.

**Note**: many tricks and workarounds are available, none of them is nice...

### Building the container locally

This command will build a container with a name that is recognised by lhc-coupling-calculation-server Spring profile.

`` podman build --tag lhc-coupling-analysis-service:local .``

<hr>

<details>
<summary>Run2 documentation for reference</summary>

## Docker
This project is uses Docker for deployments.

The base image with all the necessary dependencies is located in another project (https://gitlab.cern.ch/coupling/coupling-docker-base-image).
It has been split because the build time of the complete solution is very long since a lot of Python dependencies have to be compiled.

## Versions

Trusted versions (in case you need to rollback):
* 0.3.8: has been used in production for the first half of the 2018 run

***ALWAYS specify the version number! In the docker registry we're currently using, the tag :latest refers to an old image!***

## Deployment on cs-ccr-optics1

Full list of commands:
```
sudo su
docker ps
docker stop <lhc-coupling-analysis-service container id>
docker pull cs-ccr-ap2.cern.ch:5000/coupling/lhc-coupling-analysis-service:0.0.2
docker run --log-driver=syslog -d -it -p 60051:60051 cs-ccr-ap2.cern.ch:5000/coupling/lhc-coupling-analysis-service:0.0.2
```

Normally, the pull should be done automatically by Docker in case it cannot find the image locally. So the shortcut is:
```
docker run --log-driver=syslog -d -it -p 60051:60051 cs-ccr-ap2.cern.ch:5000/coupling/lhc-coupling-analysis-service:0.0.2
```

***ALWAYS specify the version number! In the docker registry we're currently using, the tag :latest refers to an old image!***

The versions correspond to the Git [Tags](https://gitlab.cern.ch/coupling/lhc-coupling-analysis-service/tags) in the repository.

## Some other useful docker commands
To connect to the docker container, using a bash shell:

```
docker exec -it <containerId> /bin/bash
```

The containerId can be found by using the docker command
```
docker ps
```

## Cleanup of the staging dir
Usually, the staging dir (internal on the image) is cleaned after each successful analysis. For failed analysis attempts, the intermediate files are kept for later analysis.
However, it might happen that the cleaning is not done or does not work. In these cases, the staging dir can be cleaned manually by deleting the content of it after 
connecting to the docker container as described above. The default staging dir is /tmp. So cleaning would somehow look like this:
```
docker exec -it <containerId> /bin/bash
cd /tmp
rm -rf *
```

Another option is simply, to restart the image (as seen in deployment)

</details>
