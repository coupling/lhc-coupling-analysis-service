import tempfile
import warnings
from timeit import default_timer as timer

import numpy as np
from omc3.utils.contexts import timeit
from omc3.utils.logging_tools import get_logger
from lhc_coupling_analysis import calculate_correction_analysis

LOGGER = get_logger(__name__)
RTOL = 1e-5
ATOL = 1e-5
MAX_EXPECTED_DURATION_SECONDS = 60

warnings.simplefilter(action="ignore", category=FutureWarning)  # do not pollute logs with pandas FutureWarnings


def test_coupling_server_b1():
    # This is the one that works
    # gitlab+https://gitlab.cern.ch/acc-models/acc-models-lhc@v2022.2#LHC+v2022.2&optic=R2022a_A11mC11mA10mL10m
    with tempfile.TemporaryDirectory() as session_tmp_dir:
        B1_TEST_INPUT = {
            "sdds_file": "stand_alone_test/data/trackone_b1.sdds",
            "beam": 1,
            "tune_driven_x": 0.30,
            "tune_driven_y": 0.332,
            "tune_natural_x": 0.31,
            "tune_natural_y": 0.32,
            "excitation_device": "adt",
            "mad_model_file": "stand_alone_test/acc-model.zip",
            "optic_name": "R2022a_A11mC11mA10mL10m",
            "session_tmp_dir": session_tmp_dir
        }

        B1_REAL_KNOB = -0.000993560395965
        B1_IMAG_KNOB = -0.00102251708703

        start = timer()
        with timeit(lambda x: LOGGER.info(f"Time used to compute B1 correction: {x:.2f}s")):
            correction = calculate_correction_analysis(**B1_TEST_INPUT)
        end = timer()
        time = end - start

        LOGGER.debug("Checking correction calculation fits the requirements")
        assert (
                time < MAX_EXPECTED_DURATION_SECONDS
        ), f"Maximal duration should be below {MAX_EXPECTED_DURATION_SECONDS} seconds, but was {time} seconds."
        assert np.isclose(
            correction["real"], B1_REAL_KNOB, rtol=RTOL, atol=ATOL
        ), f"real knob equals {correction['real']} wich is not close to {B1_REAL_KNOB}"
        assert np.isclose(
            correction["imaginary"], B1_IMAG_KNOB, rtol=RTOL, atol=ATOL
        ), f"imaginary knob equals {correction['imaginary']} wich is not close to {B1_IMAG_KNOB}"
        return correction


def test_coupling_server_b2():
    # This is the one that works
    # gitlab+https://gitlab.cern.ch/acc-models/acc-models-lhc@v2022.2#LHC+v2022.2&optic=R2022a_A11mC11mA10mL10m
    with tempfile.TemporaryDirectory() as session_tmp_dir:
        B2_TEST_INPUT = {
            "sdds_file": "stand_alone_test/data/trackone_b2.sdds",
            "beam": 2,
            "tune_driven_x": 0.30,
            "tune_driven_y": 0.332,
            "tune_natural_x": 0.31,
            "tune_natural_y": 0.32,
            "excitation_device": "adt",
            "mad_model_file": "stand_alone_test/acc-model.zip",
            "optic_name": "R2022a_A11mC11mA10mL10m",
            "session_tmp_dir": session_tmp_dir
        }

        B2_REAL_KNOB = 0.000990833787384
        B2_IMAG_KNOB = 0.00101105733296
        start = timer()
        with timeit(lambda x: LOGGER.info(f"Time used to compute B2 correction: {x:.2f}s")):
            correction = calculate_correction_analysis(**B2_TEST_INPUT)
        end = timer()
        time = end - start

        LOGGER.debug("Checking correction calculation fits the requirements")
        assert (
                time < MAX_EXPECTED_DURATION_SECONDS
        ), f"Maximal duration should be below {MAX_EXPECTED_DURATION_SECONDS} seconds, but was {time} seconds."
        assert np.isclose(
            correction["real"], B2_REAL_KNOB, rtol=RTOL, atol=ATOL
        ), f"real knob equals {correction['real']} wich is not close to {B2_REAL_KNOB}"
        assert np.isclose(
            correction["imaginary"], B2_IMAG_KNOB, rtol=RTOL, atol=ATOL
        ), f"imaginary knob equals {correction['imaginary']} wich is not close to {B2_IMAG_KNOB}"
        return correction


if __name__ == "__main__":
    LOGGER.info("Starting test on Beam 1")
    b1_correction = test_coupling_server_b1()

    LOGGER.info("Starting test on Beam 2")
    b2_correction = test_coupling_server_b2()

    LOGGER.info("===== Computed corrections for Beam 1 and 2 =====")
    LOGGER.info(b1_correction)
    LOGGER.info(b2_correction)
