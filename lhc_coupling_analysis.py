import os
import threading
import time
from pathlib import Path
from zipfile import ZipFile

import tfs
from omc3.global_correction import global_correction_entrypoint
from omc3.hole_in_one import hole_in_one_entrypoint
from omc3.model_creator import create_instance_and_model
from omc3.optics_measurements.constants import F1001
from omc3.response_creator import create_response_entrypoint
from omc3.utils.logging_tools import get_logger

LOGGER = get_logger(__name__)
VARIABLE_CATEGORIES = ["coupling_knobs"]
RESPONSEMATRIX = "responsematrix"
MODEL = "model"
OUTPUT_DIR = "output_dir"
OPTICS_PARAMETERS = [f"{F1001}R", f"{F1001}I"]
WEIGHTS = [1.0, 1.0]
YEAR = "2022"


def _create_model(beam, tune_natural_x, tune_natural_y, tune_driven_x, tune_driven_y, modifiers, model_dir):
    create_instance_and_model(
        accel="lhc",
        fetch = "path",
        year=YEAR,
        beam=beam,
        type="nominal",
        ats=True,
        nat_tunes=[tune_natural_x, tune_natural_y],
        drv_tunes=[tune_driven_x, tune_driven_y],
        driven_excitation="adt",
        modifiers=[Path(modifiers).absolute()],
        outputdir=Path(model_dir).absolute(),
        logfile=Path(f"{model_dir}/madx_log.txt"),
        path = Path(f"{model_dir}/acc-models-lhc")
    )


def _do_optics_analysis(beam, sdds_file, model_dir, output_dir):
    hole_in_one_entrypoint(
        optics=True,
        files=[f"{output_dir}/{sdds_file.split('/')[-1]}"],
        outputdir=Path(output_dir),
        compensation="model",
        only_coupling=True,
        three_bpm_method=True,
        three_d_excitation=False,
        union=False,
        accel="lhc",
        ats=False,
        beam=beam,
        driven_excitation=None,
        drv_tunes=None,
        model_dir=Path(model_dir),
        year=YEAR,
        coupling_pairing = 1,
    )


def _do_frequency_analysis(beam, tune_natural_x, tune_natural_y, tune_driven_x, tune_driven_y, sdds_file, output_dir):
    hole_in_one_entrypoint(
        harpy=True,
        autotunes=None,
        clean=False,
        files=[Path(sdds_file)],
        first_bpm=None,
        natdeltas=None,
        nattunes=[tune_natural_x, tune_natural_y, 0.0],
        tunes=[tune_driven_x, tune_driven_y, 0.0],
        output_bits=14,
        outputdir=Path(output_dir),
        peak_to_peak=1e-08,
        sing_val=12,
        svd_dominance_limit=0.925,
        tbt_datatype="lhc",
        to_write=["lin"],
        tolerance=0.0001,
        tune_clean_limit=2e-05,
        turn_bits=15,
        window="hann",
        optics=False,
        union=False,
        accel="lhc",
        ats=False,
        beam=beam,
        year=YEAR,
        coupling_pairing = 1
    )


def _generate_fullresponse(model_dir, beam, responsematrix):
    create_response_entrypoint(
        model_dir=Path(model_dir).absolute(),
        accel="lhc",
        year=YEAR,
        beam=beam,
        ats=True,
        delta_k=0.00002,
        variable_categories=VARIABLE_CATEGORIES,
        outfile_path=Path(responsematrix).absolute(),
    )


def _calculate_global_correction(beam, model_dir, output_dir, responsematrix):
    f1001_df = tfs.read(f"{Path(output_dir).absolute()}/f1001.tfs", index="NAME")
    mean_f1001 = f1001_df['AMP'].mean()
    std_f1001  = f1001_df['AMP'].std()
    median_f1001  = f1001_df['AMP'].median()
    modelCut    =   f1001_df['AMP'].mean() + 1.5*f1001_df['AMP'].std()
    tot_bpm_cut = f1001_df['AMP'][f1001_df['AMP'] > modelCut].count()
    LOGGER.info(f"Cutting amplitude of f1001 larger than: {modelCut}. The mean is {mean_f1001}, std {std_f1001} median: {median_f1001}")
    LOGGER.info(f"In total {tot_bpm_cut} BPMs are removed")

    global_correction_entrypoint(
        accel="lhc",
        year=YEAR,
        beam=beam,
        ats=True,
        model_dir=Path(model_dir).absolute(),
        meas_dir=Path(output_dir).absolute(),
        variable_categories=VARIABLE_CATEGORIES,
        fullresponse_path=Path(responsematrix).absolute(),
        optics_params=OPTICS_PARAMETERS,
        output_dir=Path(output_dir).absolute(),
        weights=WEIGHTS,
        svd_cut=0.01,
        iterations = 1,
        modelcut=[modelCut,modelCut]
    )


def calculate_correction_analysis(
    sdds_file,
    beam,
    tune_driven_x,
    tune_driven_y,
    tune_natural_x,
    tune_natural_y,
    excitation_device,
    mad_model_file,
    optic_name,
    session_tmp_dir,
):
    LOGGER.info(f"The main dir is {session_tmp_dir}")
    start_global_time = time.time()
    model_dir = f"{session_tmp_dir}/{MODEL}"
    responsematrix = f"{model_dir}/{RESPONSEMATRIX}"
    output_dir = f"{session_tmp_dir}/{OUTPUT_DIR}"
    os.makedirs(model_dir)
    os.makedirs(output_dir)

    madx_str = optic_name + ".madx"

    # extracting madx model zip file
    zipfile = ZipFile(mad_model_file)
    if zipfile.testzip() is not None:
        raise RuntimeError("Bad zipfile (CRC): " + mad_model_file)
    zipfile.extractall(model_dir)
    extracted_directories = os.listdir(model_dir)
    if len(extracted_directories) != 1:
        raise RuntimeError("MADX model Zip should contain exactly 1 folder. Found: " + str(extracted_directories))
    extracted_directory = extracted_directories[0]
    if not os.path.isdir(model_dir + os.sep + extracted_directory):
        raise RuntimeError("MADX model Zip first entry should be a directory: " + extracted_directory)

    os.rename(model_dir + os.sep + extracted_directory, model_dir + os.sep + "acc-models-lhc")

    modifiers = f"{model_dir}/acc-models-lhc/operation/optics/{madx_str}"

    # Create Model
    modelThread = threading.Thread(
        target=_create_model,
        args=(
            beam,
            tune_natural_x,
            tune_natural_y,
            tune_driven_x,
            tune_driven_y,
            modifiers,
            model_dir,
        ),
    )
    modelThread.start()

    # Frequency analysis
    _do_frequency_analysis(beam, tune_natural_x, tune_natural_y, tune_driven_x, tune_driven_y, sdds_file, output_dir)

    modelThread.join()

    responseThread = threading.Thread(target=_generate_fullresponse, args=(model_dir, beam, responsematrix))
    responseThread.start()
    # optics analysis
    _do_optics_analysis(beam, sdds_file, model_dir, output_dir)
    responseThread.join()

    _calculate_global_correction(beam, model_dir, output_dir, responsematrix)

    df_knob = tfs.read(f"{output_dir}/changeparameters_iter.tfs", index="NAME")
    LOGGER.info(f"Correction succesfull! Total time to calculate was: {time.time() - start_global_time}")

    return {
        "beam": beam,
        "real": df_knob.loc[f"b{beam}_re_ip7_knob", "DELTA"],
        "imaginary": df_knob.loc[f"b{beam}_im_ip7_knob", "DELTA"],
    }
