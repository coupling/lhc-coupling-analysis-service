import json
import logging
import os
import tempfile
import traceback

from lhc_coupling_analysis import calculate_correction_analysis

logging.basicConfig(level=logging.INFO)

COUPLING_MODE = os.getenv("COUPLING-ANALYSIS-MODE", "PRO")


def calculate_correction(**inputs):
    if COUPLING_MODE in ("PRO", "STANDALONE-TEST"):
        return calculate_correction_analysis(**inputs)
    elif COUPLING_MODE == "DEMO":
        logging.info("DEMO calculations simply returns dummy values")
        return {"beam": inputs["beam"], "real": 1, "imaginary": 2}
    else:
        raise RuntimeError("Unknown COUPLING-ANALYSIS-MODE %s. Specify the env variable correctly".format(COUPLING_MODE))


if __name__ == "__main__":

    logging.info("Started coupling analysis in CLI mode")
    if COUPLING_MODE == "STANDALONE-TEST":
        logging.info("Running standalone test")
        working_dir = "stand_alone_test/cli_working_dir"
    else:
        working_dir = os.getenv("COUPLING-WORKING-DIR")

    if working_dir is None or working_dir == "":
        raise RuntimeError("COUPLING-WORKING-DIR env variable not specified or empty !")

    input_file_path = working_dir + "/input.json"
    output_file_path = working_dir + "/output.json"

    try:
        logging.info("Reading input configuration from %s", input_file_path)
        with open(input_file_path, "r") as f:
            input_file = json.load(f)

        with tempfile.TemporaryDirectory(prefix="omc-service-") as session_tmp_dir:
            input_file["session_tmp_dir"] = session_tmp_dir

            logging.info("Starting calculation with inputs: %s", input_file)
            correction = calculate_correction(**input_file)
            logging.info("Correction: Real %s, Imaginary %s", str(correction["real"]), str(correction["imaginary"]))

        logging.info("Results will be written to %s", output_file_path)
        with open(output_file_path, "w") as f:
            json.dump(correction, f)

    except BaseException as exception:
        logging.exception("Exception caught during coupling analysis.")
        result = {
            "error": "".join(traceback.format_exception_only(exception)).strip(),
            "stacktrace": "".join(traceback.format_exception(exception)).strip()
        }
        with open(output_file_path, "w") as f:
            json.dump(result, f)
